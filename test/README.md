
# Front Test
 

# Stack

- VueJS
- CSS
  

# Estructura de proyecto
 
```text
.
├── src
| ├── assets
| | ├── base.css
| | ├── logo.png
| ├── components
| | ├── ButtonComponent.vue
| | ├── InputComponent.vue
| | ├── MyHeader.vue
| ├── main.js
├── .gitignore
├── .env.examples
├── package.json
├── babel.config.json
├── jsonconfig.json
├── package.json
├── vue.congif.js
```

### Archivo .env.example
Se colocan ejemplos de las variables de entorno necesarias para correr el proyecto.


Ejemplo:

```JS
VUE_APP_GIPHY=
  
```
Es éste proyecto se necesita obtener una API Key de Giphy. Posteriormente crear un  archivo .env.development.local y colocar la variable de entorno que se requiere en el proyecto.

### Métodos de App.vue
 
 Fetch a API de Giphy
```JS
getGifs() {
  let  apiKey  = process.env.VUE_APP_GIPHY;
  let  searchEndPoint  =  "https://api.giphy.com/v1/gifs/search?";
  let  limit  =  4;
  let  url  =  `${searchEndPoint}&api_key=${apiKey}&q=${this.searchItem}&limit=${limit}`;
  fetch(url)
    .then((response) => {
      return response.json();
   })
    .then((json) => {
    this.buildGifs(json); //Se llama la función para crear la url del gif
    this.storeItem(); //Se llama la función que guarda los datos de la búsqueda en localStorage
    this.getItem(); // Se llama función que trae los datos de localStorage
    this.searchItem  =  ""; // Borra el texto del input
  })
   .catch((err) => console.log(err));
},

```


```JS
buildGifs(json) {
  this.gifs  = json.data
  .map((gif) => gif.id)
  .map((gifId) => {
  return  `https://media.giphy.com/media/${gifId}/giphy.gif`;
});
},
//Función para guardar la búsqueda por localScorage
storeItem() {
  this.history.push(this.searchItem);
  localStorage.setItem("word", JSON.stringify(this.history));
  this.searchItem  =  "";
},
//Función par traer los datos de LocalSorage
getItem() {
  this.history  =  JSON.parse(localStorage.getItem("word"));
},
//Manda la url del gif seleccionado a localStorage
saveGif(id) {
  this.gifSaved.push(id)
  localStorage.setItem("word", JSON.stringify(this.gifSaved))
},
  
```

### Para correr el proyecto
- Entrar a la carpeta raíz del proyecto
- instalar dependencias: npm install
- Colocar la API Key en el archivo que corresponde. (Volver al inicio del README)
- Correr npm run serve, para levantar el proyecto
- Disfrutar de Gifs de perritos :)

